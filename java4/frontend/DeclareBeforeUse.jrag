/*
 * The JastAdd Extensible Java Compiler (http://jastadd.org) is covered
 * by the modified BSD License. You should have received a copy of the
 * modified BSD license with this compiler.
 *
 * Copyright (c) 2005-2008, Torbjorn Ekman
 * All rights reserved.
 */

import java.util.*;

aspect DeclareBeforeUse {
  inh lazy int VariableDeclaration.blockIndex();
  eq Program.getChild().blockIndex() = -1;// default value
  eq Block.getStmt(int index).blockIndex() = index;// index in stmt list of block

  syn boolean Block.declaredBeforeUse(VariableDeclaration decl, int indexUse) =
    decl.blockIndex() < indexUse;

  // TODO use inherited attr?
  public int ASTNode.varChildIndex(TypeDecl t) {
    ASTNode node = this;
    while(node != null && node.getParent() != null && node.getParent().getParent() != t) {
      node = node.getParent();
    }
    if(node == null)
      return -1;
    return t.getBodyDeclListNoTransform().getIndexOfChild(node);
  }

  public boolean TypeDecl.declaredBeforeUse(Variable decl, ASTNode use) {
    int indexDecl = ((ASTNode)decl).varChildIndex(this);
    int indexUse = use.varChildIndex(this);
    return indexDecl < indexUse;
  }

  public boolean TypeDecl.declaredBeforeUse(Variable decl, int indexUse) {
    int indexDecl = ((ASTNode)decl).varChildIndex(this);
    return indexDecl < indexUse;
  }
}
